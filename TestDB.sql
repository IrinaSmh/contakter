CREATE DATABASE  IF NOT EXISTS `kontact` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `kontact`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: kontact
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `number` varchar(45) NOT NULL,
  `lasting` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'ООО Сибинтек','46573829304',1),(2,'Фонд Канарейка','48439842954',1),(3,'Лагерь \"Устрица\"','67676732344',1),(4,'Салон \"У Марфуши\"','87875452342',0);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_has_event`
--

DROP TABLE IF EXISTS `company_has_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_has_event` (
  `company_id` int NOT NULL,
  `event_id` bigint NOT NULL,
  PRIMARY KEY (`company_id`,`event_id`),
  KEY `fk_company_has_event_event1_idx` (`event_id`),
  KEY `fk_company_has_event_company1_idx` (`company_id`),
  CONSTRAINT `fk_company_has_event_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_company_has_event_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_has_event`
--

LOCK TABLES `company_has_event` WRITE;
/*!40000 ALTER TABLE `company_has_event` DISABLE KEYS */;
INSERT INTO `company_has_event` VALUES (1,2),(1,3),(1,4),(1,5);
/*!40000 ALTER TABLE `company_has_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_contacts_user1_idx` (`user_id`),
  CONSTRAINT `fk_contacts_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'http://manya.vk.com',1),(2,'http://luna.inst.com',2),(3,'http://vasya.rt.com',3),(4,'http://mdcn.com',4);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `document` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `company_id` int NOT NULL,
  `type_of_document_id` bigint NOT NULL,
  PRIMARY KEY (`id`,`company_id`,`type_of_document_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_document_company1_idx` (`company_id`),
  KEY `fk_document_type_of_document1_idx` (`type_of_document_id`),
  CONSTRAINT `fk_document_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_document_type_of_document1` FOREIGN KEY (`type_of_document_id`) REFERENCES `type_of_document` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'путь1',1,1),(2,'путь2',2,1),(3,'путь3',3,1),(4,'путь4',4,2),(5,'путь5',1,2),(6,'путь6',2,2),(7,'путь7',3,2),(8,'путь8',4,3),(9,'путь9',1,3),(10,'путь10',2,3);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'Мероприятие1','Описание1','2021-08-01','Место1','16:59:59'),(2,'Мероприятие2','Описание2','2021-08-23','Место2','16:59:59'),(3,'Мероприятие3','Описание3','2021-08-24','Место3','16:59:59'),(4,'Мероприятие4','Описание4','2021-09-03','Место4','16:59:59'),(5,'Мероприятие5','Описание5','2021-09-04','Место5','16:59:59'),(6,'Мероприятие6','Описание6','2021-09-04','Место6','16:59:59'),(7,'Мероприятие7','Описание7','2021-09-04','Место7','16:59:59'),(8,'Мероприятие8','Описание8','2021-10-05','Место8','16:59:59'),(9,'Мероприятие9','Описание9','2021-10-06','Место9','16:59:59'),(10,'Мероприятие10','Описание10','2021-10-07','Место10','16:59:59');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pay_information`
--

DROP TABLE IF EXISTS `pay_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pay_information` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `status` tinyint NOT NULL,
  `company_id` int NOT NULL,
  `type_of_pay_id` bigint NOT NULL,
  PRIMARY KEY (`id`,`company_id`,`type_of_pay_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_pay_information_company1_idx` (`company_id`),
  KEY `fk_pay_information_type_of_pay1_idx` (`type_of_pay_id`),
  CONSTRAINT `fk_pay_information_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_pay_information_type_of_pay1` FOREIGN KEY (`type_of_pay_id`) REFERENCES `type_of_pay` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pay_information`
--

LOCK TABLES `pay_information` WRITE;
/*!40000 ALTER TABLE `pay_information` DISABLE KEYS */;
INSERT INTO `pay_information` VALUES (1,1,1,1),(2,1,1,2),(3,1,1,3),(4,1,2,1),(5,0,2,2),(6,0,2,3),(7,0,3,1),(8,1,3,2),(9,1,3,3),(10,1,4,1),(11,1,4,2),(12,1,4,3);
/*!40000 ALTER TABLE `pay_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_user`
--

DROP TABLE IF EXISTS `role_has_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_user` (
  `role_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_role_has_user_user1_idx` (`user_id`),
  KEY `fk_role_has_user_role_idx` (`role_id`),
  CONSTRAINT `fk_role_has_user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_role_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_user`
--

LOCK TABLES `role_has_user` WRITE;
/*!40000 ALTER TABLE `role_has_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_of_document`
--

DROP TABLE IF EXISTS `type_of_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_of_document` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_of_document`
--

LOCK TABLES `type_of_document` WRITE;
/*!40000 ALTER TABLE `type_of_document` DISABLE KEYS */;
INSERT INTO `type_of_document` VALUES (1,'1 тип'),(2,'2тип'),(3,'3тип'),(4,'4тип'),(5,'5тип'),(6,'6тип'),(7,'7тип');
/*!40000 ALTER TABLE `type_of_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_of_pay`
--

DROP TABLE IF EXISTS `type_of_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_of_pay` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_of_pay`
--

LOCK TABLES `type_of_pay` WRITE;
/*!40000 ALTER TABLE `type_of_pay` DISABLE KEYS */;
INSERT INTO `type_of_pay` VALUES (1,'оплата коммуналки'),(2,'оплата аренды'),(3,'оплата уборки');
/*!40000 ALTER TABLE `type_of_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(45) NOT NULL,
  `company_id` int NOT NULL,
  `createAt` timestamp(6) NOT NULL,
  PRIMARY KEY (`id`,`company_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_company1_idx` (`company_id`),
  CONSTRAINT `fk_user_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'kira@yandex.ru','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','kira',1,'2020-12-31 21:00:00.000000'),(2,'guru@gmail.com','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','guru',2,'2020-12-31 21:00:00.000000'),(3,'zero@mail.ru','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','zero',3,'2020-12-31 21:00:00.000000'),(4,'umnik@gmail.com','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','umnik',4,'2020-12-31 21:00:00.000000'),(5,'yuoyu.gmail.com','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','yuoyu',1,'2020-12-31 21:00:00.000000'),(6,'beit.gmail.com','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','beit',2,'2020-12-31 21:00:00.000000'),(7,'dom@dom.ru','$2a$10$wqvNHC1Acn3AAaCH3XMo.uH9nTsxfyoDgBPOMf.RsAxIOe/jcBYNe','dom',3,'2020-12-31 21:00:00.000000');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21 20:29:11
