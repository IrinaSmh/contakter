package com.vstu.megaguren.kontactor.dao;

import com.vstu.megaguren.kontactor.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);
}