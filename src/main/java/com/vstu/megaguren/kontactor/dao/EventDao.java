package com.vstu.megaguren.kontactor.dao;

import com.vstu.megaguren.kontactor.model.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventDao extends CrudRepository<Event, Long> {
    Optional<List<Event>> findAllByDateIsGreaterThan(LocalDate date);

    Optional<List<Event>> findAllByCompanies_Id(Long companyId);
}
