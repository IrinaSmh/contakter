package com.vstu.megaguren.kontactor.dao;

import com.vstu.megaguren.kontactor.model.PayInformation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PayInformationDao extends CrudRepository<PayInformation, Long> {
    Optional<List<PayInformation>> findAllByCompany_Id(Long companyId);
}

