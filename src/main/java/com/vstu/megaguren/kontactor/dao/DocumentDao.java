package com.vstu.megaguren.kontactor.dao;

import com.vstu.megaguren.kontactor.model.Company;
import com.vstu.megaguren.kontactor.model.Document;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentDao extends CrudRepository<Document, Long> {
    Optional<List<Document>> findAllByCompany_Id(Long companyId);
}
