package com.vstu.megaguren.kontactor.dao;

import com.vstu.megaguren.kontactor.model.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyDao extends CrudRepository<Company, Long> {
    Optional<Company> getByUsers_Email(String username);
}
