package com.vstu.megaguren.kontactor.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vstu.megaguren.kontactor.dto.UserDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "USER", uniqueConstraints = @UniqueConstraint(columnNames = "EMAIL"))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "CREATEAT", nullable = false)
    @CreationTimestamp
    private LocalDateTime createAt;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE ,CascadeType.PERSIST})
    @JoinTable(
            name = "ROLE_HAS_USER",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")
    )
    private List<Role> roles;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    private Company company;


    public User(UserDto userDto) {
        this.id = userDto.getId();
        this.name = userDto.getName();
        this.email = userDto.getEmail();
        this.password = userDto.getPassword();
        this.createAt = userDto.getCreateAt();
    }

}