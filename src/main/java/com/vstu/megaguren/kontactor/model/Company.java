package com.vstu.megaguren.kontactor.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "COMPANY", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME", "NUMBER"}))
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "NUMBER", nullable = false)
    private String number;

    @Column(name = "LASTING", nullable = false)
    private Boolean lasting;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE ,CascadeType.PERSIST}, mappedBy = "company")
    @JsonBackReference
    private List<User> users;
}
