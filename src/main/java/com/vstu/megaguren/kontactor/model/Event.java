package com.vstu.megaguren.kontactor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "EVENT")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "DATE", nullable = false)
    private LocalDate date;

    @Column(name = "TIME", nullable = false)
    private LocalTime time;

    @Column(name = "PLACE", nullable = false)
    private String place;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE ,CascadeType.PERSIST})
    @JoinTable(
            name = "COMPANY_HAS_EVENT",
            joinColumns = @JoinColumn(name = "EVENT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "COMPANY_ID", referencedColumnName = "ID")
    )
    private List<Company> companies;
}
