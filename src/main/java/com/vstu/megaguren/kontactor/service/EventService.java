package com.vstu.megaguren.kontactor.service;

import com.vstu.megaguren.kontactor.dao.EventDao;
import com.vstu.megaguren.kontactor.dto.EventDto;
import com.vstu.megaguren.kontactor.exception.EventNotFoundException;
import com.vstu.megaguren.kontactor.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {
    private EventDao eventDao;
    private CompanyService companyService;

    @Autowired
    public EventService(EventDao eventDao, CompanyService companyService){
        this.eventDao = eventDao;
        this.companyService = companyService;
    }

    public List<EventDto> getAllCurrentEvents(){
        LocalDate currentDate = LocalDate.now();
        List<Event> events = eventDao.findAllByDateIsGreaterThan(currentDate).orElseThrow(
                ()->new EventNotFoundException("Current events with date "
            + currentDate.toString() + " not found"));

        List<EventDto> result = new ArrayList<>();
        events.forEach(event -> {
            result.add(new EventDto(event));
        });

        return result;
    }

    public List<EventDto> getAllEventsByCurrentResident() {
        Long companyId = companyService.getCompanyByCurrentResident().getId();
        List<Event> events = eventDao.findAllByCompanies_Id(companyService.getCompanyByCurrentResident().getId()).orElseThrow(
                ()->new EventNotFoundException("Events of company with id "
                        + companyId.toString() + " not found"));

        List<EventDto> result = new ArrayList<>();
        events.forEach(event -> {
            result.add(new EventDto(event));
        });

        return result;
    }
}
