package com.vstu.megaguren.kontactor.service;

import com.vstu.megaguren.kontactor.dao.CompanyDao;
import com.vstu.megaguren.kontactor.dao.DocumentDao;
import com.vstu.megaguren.kontactor.dao.PayInformationDao;
import com.vstu.megaguren.kontactor.dto.CompanyDto;
import com.vstu.megaguren.kontactor.dto.DocumentDto;
import com.vstu.megaguren.kontactor.dto.PayInformationDto;
import com.vstu.megaguren.kontactor.exception.CompanyNotFoundException;
import com.vstu.megaguren.kontactor.exception.DocumentNotFoundException;
import com.vstu.megaguren.kontactor.exception.PayInfromationNotFoundException;
import com.vstu.megaguren.kontactor.model.Company;
import com.vstu.megaguren.kontactor.model.Document;
import com.vstu.megaguren.kontactor.model.PayInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyService {
    private CompanyDao companyDao;
    private DocumentDao documentDao;
    private PayInformationDao payInformationDao;

    @Autowired
    public CompanyService(CompanyDao companyDao, DocumentDao documentDao, PayInformationDao payInformationDao) {
        this.companyDao = companyDao;
        this.documentDao = documentDao;
        this.payInformationDao = payInformationDao;
    }

    public CompanyDto getCompanyByCurrentResident(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Company company = companyDao.getByUsers_Email(userDetails.getUsername()).orElseThrow(()->
                new CompanyNotFoundException("Company for user with name " + userDetails.getUsername() + " not found")
        );
        return new CompanyDto(company);
    }

    public List<DocumentDto> getAllDocumentsByCurrentResident(){
        CompanyDto company = getCompanyByCurrentResident();
        List<Document> documents = documentDao.findAllByCompany_Id(company.getId()).orElseThrow(()->
                new DocumentNotFoundException("Document for company " + company.getName() + " not found"));
        List<DocumentDto> documentDtoList = new ArrayList<>();
        documents.forEach(document -> {
            documentDtoList.add(new DocumentDto(document));
        });
        return documentDtoList;
    }

    public List<PayInformationDto> getAllPayInformationForCurrentCompany(){
        CompanyDto company = getCompanyByCurrentResident();
        List<PayInformation> payInformations = payInformationDao.findAllByCompany_Id(company.getId())
                .orElseThrow(()-> new PayInfromationNotFoundException("Pay information for company "+ company.getName() + " not found"));
        List<PayInformationDto> payInformationDtos = new ArrayList<>();
        payInformations.forEach(payInformation -> {
            payInformationDtos.add(new PayInformationDto(payInformation));
        });
        return payInformationDtos;
    }
}
