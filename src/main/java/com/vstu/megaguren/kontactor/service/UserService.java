package com.vstu.megaguren.kontactor.service;

import com.vstu.megaguren.kontactor.dao.UserDao;
import com.vstu.megaguren.kontactor.dto.UserDto;
import com.vstu.megaguren.kontactor.exception.UserNotFoundException;
import com.vstu.megaguren.kontactor.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private UserDao userDao;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto getAuthUserCredentials() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getUserByEmail(userDetails.getUsername());
    }

    public UserDto saveUser(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = new User(userDto);
        return new UserDto(userDao.save(user));
    }

    public UserDto getUserByEmail(String email) {
        return new UserDto(userDao.findByEmail(email).orElseThrow(() ->
                new UserNotFoundException("User with email " + email + "not found.")));
    }

    public UserDto getUserById(Long id) {
        return new UserDto(userDao.findById(id).orElseThrow(() ->
                new UserNotFoundException("User with id " + id + "not found.")));
    }


    @Transactional
    public void updatePassword(String password, Long id) {
        User user = userDao.findById(id).orElseThrow(() -> new UserNotFoundException("User with " + id + " not found"));
        user.setPassword(passwordEncoder.encode(password));
        userDao.save(user);
    }

    @Transactional
    public void delete(Long id) {
        User user = userDao.findById(id).orElseThrow(() -> new UserNotFoundException("User with " + id + " not found"));
        userDao.delete(user);
    }
}
