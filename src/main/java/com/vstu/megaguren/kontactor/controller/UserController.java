package com.vstu.megaguren.kontactor.controller;

import com.vstu.megaguren.kontactor.dto.UserDto;
import com.vstu.megaguren.kontactor.exception.UserNotFoundException;
import com.vstu.megaguren.kontactor.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users")
@Api("/users")
public class UserController {
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public ResponseEntity<UserDto> getAuthUserCredentials() {
        try {
            return new ResponseEntity<>(userService.getAuthUserCredentials(), HttpStatus.OK);
        } catch (UsernameNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/getByEmail/{email}")
    @ApiOperation(value = "Возвращает пользователя по эл. почте")
    public ResponseEntity<UserDto> getUserByEmail(@PathVariable(value = "email") String email) {
        try {
            return new ResponseEntity<>(userService.getUserByEmail(email), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<UserDto> save(@RequestBody UserDto userDto) {
        return new ResponseEntity<>(userService.saveUser(userDto), HttpStatus.CREATED);
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Возвращает пользователя по id")
    public ResponseEntity<UserDto> getUserById(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping(value = "/password")
    public void updatePassword(String password) {
        userService.updatePassword(password, userService.getAuthUserCredentials().getId());
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity delete() {
        try {
            userService.delete(userService.getAuthUserCredentials().getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
