package com.vstu.megaguren.kontactor.controller;

import com.vstu.megaguren.kontactor.dto.EventDto;
import com.vstu.megaguren.kontactor.dto.UserDto;
import com.vstu.megaguren.kontactor.exception.EventNotFoundException;
import com.vstu.megaguren.kontactor.service.EventService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/events")
@Api("/events")
public class EventController {
    private EventService eventService;

    @Autowired
    public EventController(EventService eventService){
        this.eventService = eventService;
    }

    @GetMapping("/allCurrent")
    public ResponseEntity<List<EventDto>> getAllCurrentEvents() {
        try {
            return new ResponseEntity<>(eventService.getAllCurrentEvents(), HttpStatus.OK);
        } catch (EventNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/all")
    public ResponseEntity<List<EventDto>> getAllEventsForCurrentCompany() {
        try {
            return new ResponseEntity<>(eventService.getAllEventsByCurrentResident(), HttpStatus.OK);
        } catch (EventNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
