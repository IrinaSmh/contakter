package com.vstu.megaguren.kontactor.controller;

import com.vstu.megaguren.kontactor.dto.CompanyDto;
import com.vstu.megaguren.kontactor.dto.DocumentDto;
import com.vstu.megaguren.kontactor.dto.EventDto;
import com.vstu.megaguren.kontactor.dto.PayInformationDto;
import com.vstu.megaguren.kontactor.exception.EventNotFoundException;
import com.vstu.megaguren.kontactor.model.Document;
import com.vstu.megaguren.kontactor.service.CompanyService;
import com.vstu.megaguren.kontactor.service.EventService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/company")
@Api("/company")
public class CompanyController {
    private CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService){
        this.companyService = companyService;
    }

    @GetMapping("/this")
    public ResponseEntity<CompanyDto> getCurrentCompany() {
        try {
            return new ResponseEntity<>(companyService.getCompanyByCurrentResident(), HttpStatus.OK);
        } catch (EventNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/documents")
    public ResponseEntity<List<DocumentDto>> getAllDocumentsForCurrentCompany() {
        try {
            return new ResponseEntity<>(companyService.getAllDocumentsByCurrentResident(), HttpStatus.OK);
        } catch (EventNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/pay_info")
    public ResponseEntity<List<PayInformationDto>> getAllPayInfoForCurrentCompany() {
        try {
            return new ResponseEntity<>(companyService.getAllPayInformationForCurrentCompany(), HttpStatus.OK);
        } catch (EventNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
