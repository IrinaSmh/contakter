package com.vstu.megaguren.kontactor.exception;

public class PayInfromationNotFoundException extends RuntimeException {
    public PayInfromationNotFoundException(String message) {
        super(message);
    }
}
