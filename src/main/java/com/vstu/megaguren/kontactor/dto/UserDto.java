package com.vstu.megaguren.kontactor.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.vstu.megaguren.kontactor.model.Company;
import com.vstu.megaguren.kontactor.model.Role;
import com.vstu.megaguren.kontactor.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@ToString
@Data
public class UserDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotNull
    private LocalDateTime createAt;

    private List<Role> roles;

    private Company company;

    public UserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.createAt = user.getCreateAt();
        this.roles = user.getRoles();
        this.company = user.getCompany();
    }

}