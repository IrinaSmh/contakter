package com.vstu.megaguren.kontactor.dto;

import com.sun.istack.NotNull;
import com.vstu.megaguren.kontactor.model.PayInformation;
import com.vstu.megaguren.kontactor.model.TypeOfPay;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Data
public class PayInformationDto {
    private Long id;

    @NotNull
    private Boolean status;

    private TypeOfPay typeOfPay;

    public PayInformationDto(PayInformation payInformation){
        this.id = payInformation.getId();
        this.status = payInformation.getStatus();
        this.typeOfPay = payInformation.getTypeOfPay();
    }
}
