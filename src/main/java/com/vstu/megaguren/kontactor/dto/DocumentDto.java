package com.vstu.megaguren.kontactor.dto;

import com.sun.istack.NotNull;
import com.vstu.megaguren.kontactor.model.Document;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@Data
public class DocumentDto {
    private Long id;

    @NotNull
    private String file;

    public DocumentDto(Document document){
        this.file = document.getFile();
        this.id = document.getId();
    }
}
