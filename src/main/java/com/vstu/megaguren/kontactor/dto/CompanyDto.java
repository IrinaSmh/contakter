package com.vstu.megaguren.kontactor.dto;

import com.sun.istack.NotNull;
import com.vstu.megaguren.kontactor.model.Company;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@ToString
@Data
public class CompanyDto {
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String number;

    @NotNull
    private Boolean isLasting;

    private List<UserDto> users;

    public CompanyDto(Company company){
        this.id = company.getId();
        this.name = company.getName();
        this.number = company.getNumber();
        this.isLasting = company.getLasting();
        List<UserDto> usersDto = new ArrayList<>();
        company.getUsers().forEach(user->usersDto.add(new UserDto(user)));
        this.users = usersDto;
    }
}
