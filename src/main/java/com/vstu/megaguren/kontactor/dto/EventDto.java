package com.vstu.megaguren.kontactor.dto;

import com.sun.istack.NotNull;
import com.vstu.megaguren.kontactor.model.Company;
import com.vstu.megaguren.kontactor.model.Event;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@ToString
@Data
public class EventDto {
    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime time;

    @NotNull
    private String place;

    private List<String> companyNames;

    public EventDto(Event event){
        this.id = event.getId();
        this.name = event.getName();
        this.description = event.getDescription();
        this.date = event.getDate();
        this.time = event.getTime();
        this.place = event.getPlace();

        if(!event.getCompanies().isEmpty()) {
            List<String> namesString = new ArrayList<>();
            event.getCompanies().forEach(company->{
                namesString.add(company.getName());
            });
            this.companyNames = namesString;
        }
    }
}
